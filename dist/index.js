"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const controllers_1 = require("./controllers");
const utils_1 = require("./utils");
const app = express_1.default();
app.use("/welcome", controllers_1.HelloController);
const PORT = utils_1.getAppPort();
app.listen(PORT, () => {
    console.log(`Listening for trivial app service at http://127.0.0.1:${PORT}`);
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxzREFBOEI7QUFDOUIsK0NBQWdEO0FBQ2hELG1DQUFxQztBQUVyQyxNQUFNLEdBQUcsR0FBd0IsaUJBQU8sRUFBRSxDQUFDO0FBRTNDLEdBQUcsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLDZCQUFlLENBQUMsQ0FBQztBQUVyQyxNQUFNLElBQUksR0FBRyxrQkFBVSxFQUFFLENBQUM7QUFFMUIsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsR0FBRyxFQUFFO0lBQ3BCLE9BQU8sQ0FBQyxHQUFHLENBQUMseURBQXlELElBQUksRUFBRSxDQUFDLENBQUM7QUFDL0UsQ0FBQyxDQUFDLENBQUMifQ==