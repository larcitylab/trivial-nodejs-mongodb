"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// The beginnings of your mongoose connection file
const utils_1 = require("./utils");
const connectionURL = utils_1.getDatabaseConnectionURL();
console.log(`Connection URL: ${connectionURL}`);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9uZ29vc2UuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvbW9uZ29vc2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxrREFBa0Q7QUFDbEQsbUNBQW1EO0FBRW5ELE1BQU0sYUFBYSxHQUFHLGdDQUF3QixFQUFFLENBQUM7QUFFakQsT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsYUFBYSxFQUFFLENBQUMsQ0FBQyJ9