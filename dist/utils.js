"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_1 = __importDefault(require("lodash"));
exports.getDatabaseConnectionURL = () => {
    let mongoURL = process.env.OPENSHIFT_MONGODB_DB_URL || process.env.MONGO_URL;
    // let mongoURLLabel = "";
    if (!lodash_1.default.isEmpty(mongoURL)) {
        return mongoURL;
    }
    // Connection string is empty
    let mongoHost, mongoPort, mongoDatabase, mongoPassword, mongoUser;
    // If using plane old env vars via service discovery
    if (process.env.DATABASE_SERVICE_NAME) {
        var mongoServiceName = process.env.DATABASE_SERVICE_NAME.toUpperCase();
        mongoHost = process.env[mongoServiceName + "_SERVICE_HOST"];
        mongoPort = process.env[mongoServiceName + "_SERVICE_PORT"];
        mongoDatabase = process.env[mongoServiceName + "_DATABASE"];
        mongoPassword = process.env[mongoServiceName + "_PASSWORD"];
        mongoUser = process.env[mongoServiceName + "_USER"];
        // If using env vars from secret from service binding
    }
    else if (process.env.database_name) {
        mongoDatabase = process.env.database_name;
        mongoPassword = process.env.password;
        mongoUser = process.env.username;
        let mongoUriParts = process.env.uri && process.env.uri.split("//");
        if (Array.isArray(mongoUriParts) && mongoUriParts.length == 2) {
            mongoUriParts = mongoUriParts[1].split(":");
            if (mongoUriParts && mongoUriParts.length == 2) {
                mongoHost = mongoUriParts[0];
                mongoPort = mongoUriParts[1];
            }
        }
    }
    if (mongoHost && mongoPort && mongoDatabase) {
        // mongoURLLabel = mongoURL = "mongodb://";
        mongoURL = "mongodb://";
        if (mongoUser && mongoPassword) {
            mongoURL += mongoUser + ":" + mongoPassword + "@";
        }
        // Provide UI label that excludes user id and pw
        // mongoURLLabel += mongoHost + ":" + mongoPort + "/" + mongoDatabase;
        mongoURL += mongoHost + ":" + mongoPort + "/" + mongoDatabase;
    }
    return mongoURL;
};
exports.getAppPort = () => {
    return process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || "8080";
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXRpbHMuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi9zcmMvdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxvREFBdUI7QUFFVixRQUFBLHdCQUF3QixHQUFHLEdBQUcsRUFBRTtJQUMzQyxJQUFJLFFBQVEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLHdCQUF3QixJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDO0lBQzdFLDBCQUEwQjtJQUUxQixJQUFJLENBQUMsZ0JBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEVBQUU7UUFDeEIsT0FBTyxRQUFRLENBQUM7S0FDakI7SUFFRCw2QkFBNkI7SUFDN0IsSUFBSSxTQUFTLEVBQUUsU0FBUyxFQUFFLGFBQWEsRUFBRSxhQUFhLEVBQUUsU0FBUyxDQUFDO0lBRWxFLG9EQUFvRDtJQUNwRCxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUU7UUFDckMsSUFBSSxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3ZFLFNBQVMsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLGVBQWUsQ0FBQyxDQUFDO1FBQzVELFNBQVMsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLGVBQWUsQ0FBQyxDQUFDO1FBQzVELGFBQWEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLFdBQVcsQ0FBQyxDQUFDO1FBQzVELGFBQWEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLFdBQVcsQ0FBQyxDQUFDO1FBQzVELFNBQVMsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixHQUFHLE9BQU8sQ0FBQyxDQUFDO1FBRXBELHFEQUFxRDtLQUN0RDtTQUFNLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUU7UUFDcEMsYUFBYSxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDO1FBQzFDLGFBQWEsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQztRQUNyQyxTQUFTLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUM7UUFDakMsSUFBSSxhQUFhLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25FLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxhQUFhLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtZQUM3RCxhQUFhLEdBQUcsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM1QyxJQUFJLGFBQWEsSUFBSSxhQUFhLENBQUMsTUFBTSxJQUFJLENBQUMsRUFBRTtnQkFDOUMsU0FBUyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDN0IsU0FBUyxHQUFHLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUM5QjtTQUNGO0tBQ0Y7SUFFRCxJQUFJLFNBQVMsSUFBSSxTQUFTLElBQUksYUFBYSxFQUFFO1FBQzNDLDJDQUEyQztRQUMzQyxRQUFRLEdBQUcsWUFBWSxDQUFDO1FBQ3hCLElBQUksU0FBUyxJQUFJLGFBQWEsRUFBRTtZQUM5QixRQUFRLElBQUksU0FBUyxHQUFHLEdBQUcsR0FBRyxhQUFhLEdBQUcsR0FBRyxDQUFDO1NBQ25EO1FBQ0QsZ0RBQWdEO1FBQ2hELHNFQUFzRTtRQUN0RSxRQUFRLElBQUksU0FBUyxHQUFHLEdBQUcsR0FBRyxTQUFTLEdBQUcsR0FBRyxHQUFHLGFBQWEsQ0FBQztLQUMvRDtJQUVELE9BQU8sUUFBUSxDQUFDO0FBQ2xCLENBQUMsQ0FBQztBQUVXLFFBQUEsVUFBVSxHQUFHLEdBQUcsRUFBRTtJQUM3QixPQUFPLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLElBQUksTUFBTSxDQUFDO0FBQ3pFLENBQUMsQ0FBQyJ9