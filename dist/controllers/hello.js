"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
/**
 * An example middleware function
 */
const showRequestURLMiddleware = (req, res, nxt) => {
    console.log(`URL: ${req.originalUrl}`);
    nxt();
};
/**
 * An example contorller endpoint
 */
router.get("/", showRequestURLMiddleware, (req, res) => {
    res.send("Hello, World!");
});
/**
 * An example controller endpoint
 */
router.get("/:name", showRequestURLMiddleware, (req, res) => {
    let { name } = req.params;
    res.send(`Hello ${name}`);
});
exports.HelloController = router;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVsbG8uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvY29udHJvbGxlcnMvaGVsbG8udHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSxxQ0FBa0U7QUFFbEUsTUFBTSxNQUFNLEdBQVcsZ0JBQU0sRUFBRSxDQUFDO0FBRWhDOztHQUVHO0FBQ0gsTUFBTSx3QkFBd0IsR0FBRyxDQUMvQixHQUFZLEVBQ1osR0FBYSxFQUNiLEdBQWlCLEVBQ2pCLEVBQUU7SUFDRixPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7SUFDdkMsR0FBRyxFQUFFLENBQUM7QUFDUixDQUFDLENBQUM7QUFFRjs7R0FFRztBQUNILE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLHdCQUF3QixFQUFFLENBQUMsR0FBWSxFQUFFLEdBQWEsRUFBRSxFQUFFO0lBQ3hFLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7QUFDNUIsQ0FBQyxDQUFDLENBQUM7QUFFSDs7R0FFRztBQUNILE1BQU0sQ0FBQyxHQUFHLENBQ1IsUUFBUSxFQUNSLHdCQUF3QixFQUN4QixDQUFDLEdBQVksRUFBRSxHQUFhLEVBQUUsRUFBRTtJQUM5QixJQUFJLEVBQUUsSUFBSSxFQUFFLEdBQUcsR0FBRyxDQUFDLE1BQU0sQ0FBQztJQUMxQixHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsSUFBSSxFQUFFLENBQUMsQ0FBQztBQUM1QixDQUFDLENBQ0YsQ0FBQztBQUVXLFFBQUEsZUFBZSxHQUFXLE1BQU0sQ0FBQyJ9