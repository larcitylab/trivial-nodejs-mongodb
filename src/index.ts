import express from "express";
import { HelloController } from "./controllers";
import { getAppPort } from "./utils";

const app: express.Application = express();

app.use("/welcome", HelloController);

const PORT = getAppPort();

app.listen(PORT, () => {
  console.log(`Listening for trivial app service at http://127.0.0.1:${PORT}`);
});
