// The beginnings of your mongoose connection file
import { getDatabaseConnectionURL } from "./utils";

const connectionURL = getDatabaseConnectionURL();

console.log(`Connection URL: ${connectionURL}`);
