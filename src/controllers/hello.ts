import { Router, Request, Response, NextFunction } from "express";

const router: Router = Router();

/**
 * An example middleware function
 */
const showRequestURLMiddleware = (
  req: Request,
  res: Response,
  nxt: NextFunction
) => {
  console.log(`URL: ${req.originalUrl}`);
  nxt();
};

/**
 * An example contorller endpoint
 */
router.get("/", showRequestURLMiddleware, (req: Request, res: Response) => {
  res.send("Hello, World!");
});

/**
 * An example controller endpoint
 */
router.get(
  "/:name",
  showRequestURLMiddleware,
  (req: Request, res: Response) => {
    let { name } = req.params;
    res.send(`Hello ${name}`);
  }
);

export const HelloController: Router = router;
