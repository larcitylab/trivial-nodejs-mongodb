import { getAppPort, getDatabaseConnectionURL } from "./utils";

describe("WelcomeController", () => {
  beforeEach(async () => {
    console.log(`Does stuff before each test...`);
  });

  it("has access to ENV DB name", () => {
    console.log(`Database name:: ${process.env.MONGODB_DATABASE_NAME}`);
    console.log(`Database URI:: ${process.env.MONGODB_URI}`);
    console.log(`Database Username:: ${process.env.MONGODB_USERNAME}`);
  });

  it("can access connection URL", () => {
    console.log(`Connection URL (env):: ${getDatabaseConnectionURL()}`);
  });

  it("can access the app port", () => {
    console.log(`Application port (env):: ${getAppPort()}`);
  });
});
