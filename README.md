# Getting Started with the OpenShift Pipeline

- You need to make sure you include every dependency referenced in your `package.json` scripts - **especially dependencies of your `test` script** - in the `dependencies` section, because the `devDependencies` get pruned before your app's unit tests are run in the OpenShift build flow.

  ```bash
  # an example error, in this case thrown by the dependency graph mocha > ts-node > typescript
  Cannot find module 'typescript'
  ```

- Ensure that if you have specified engine requirements in your `package.json` file, that they align with your service configuration in OpenShift. Such a definition looks like this:

  ```js
  //...other configurations
  "engines": {
    "node": 8,
    "npm": 6
  }
  //... other configurations
  ```

  preferrably, you should set this to be flexible to your service configurations (or leave out the `engines` section altogether), e.g.

  ```js
  //...other configurations
  "engines": {
    "node": "*",
    "npm": "*"
  }
  //... other configurations
  ```
